#!/usr/bin/python3
##############################################################################
# hitdownloader, v0.1.0 - Downloads videos and metadata from hitbdsm.com.
#
# Copyright (C) 2022 MeanMrMustardGas <meanmrmustardgas at protonmail dot com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################
import argparse
import os
import datetime
import cloudscraper
import re
import sys
from xml.sax.saxutils import escape
from pathlib import Path
from bs4 import BeautifulSoup
from http.cookiejar import MozillaCookieJar
from tqdm import tqdm

# Set up command line arguments
parser = argparse.ArgumentParser(description=''' Download 
                                                 videos from hitbdsm.com''')
parser.add_argument("url", help="URL to download.")
parser.add_argument("-q", "--quality", help="Select video quality.",
                    choices=["480p", "240p"], default="480p")
parser.add_argument("--no-video", action="store_true", default=False,
                    help="Don't download shoot video[s].")
parser.add_argument("-m", "--no-metadata", action="store_true", default=False,
                    help="Don't download any additional metadata,")
parser.add_argument("-n", "--no-nfo", action="store_true", default=False,
                    help="Don't create emby-compatible nfo file.")
parser.add_argument("-p", "--no-poster", action="store_true", default=False,
                    help="Don't download shoot poster image.")
parser.add_argument("-r", "--recursive", action="store_true", default=False,
                    help="Recursively download whole gallery.")
args = parser.parse_args()

session = cloudscraper.CloudScraper()


def get_html(url):
    """
    Gets html for processing.
    """
    req = session.get(url)
    soup = BeautifulSoup(req.text, "html.parser")
    return soup


def get_shoot_url_list(soup):
    shoot_list = []
    articles = soup.find("div", "videos-list").find_all("a")
    for a in articles:
        shoot_list.append(a['href'])
    return(shoot_list)


def get_download_urls(soup):
    player_url = soup.find_all("iframe")[1]['src']
    soup = get_html(player_url)
    video_urls = {"480p": soup.find("video").find("source", {"label": "480p"})['src'],
                  "240p": soup.find("video").find("source", {"label": "240p"})['src']}
    video_urls['poster'] = soup.find("video")['poster']
    print(video_urls)
    return video_urls


def get_metadata(soup):
    """
    Parse shoot HTML for metadata.
    """
    title = soup.find("h1", "entry-title").text
    desc = soup.find("div", "desc more").find("p").get_text()
    channel = soup.find("div", "tags-list").find("a").get_text()
    actors = []
    for actor in soup.find("div", {"id": "video-actors"}).find_all("a"):
        actors.append(actor.get_text())

    metadata = {
            "title": title,
            "desc": desc,
            "actors": actors,
            "channel": channel}
    return metadata


def write_metadata_nfo(metadata, fname):
    """
    Write metadata to emby compatible NFO file.
    """
    with open(fname, "w") as nfo:
        nfo.write('<?xml version="1.0" encoding="utf-8" standalone="yes"?>\n')
        nfo.write("<movie>\n")
        nfo.write("  <plot>" + escape(metadata['desc']) + "</plot>\n")
        nfo.write("  <title>" + escape(metadata['title']) + "</title>\n")
        nfo.write("  <studio>" + escape(metadata['channel']) + "</studio>\n")
        for i, actor in enumerate(metadata['actors']):
            nfo.write("  <actor>\n")
            nfo.write("    <name>" + escape(actor.strip()) + "</name>\n")
            nfo.write("    <type>Actor</type>\n")
            nfo.write("  </actor>\n")
        nfo.write("</movie>\n")


def download_file(url, fname):
    """
    Download url and save as filename with progressbar
    """
    fpath = Path(fname)

    if fpath.exists():
        r = session.head(url)
        length = r.headers['Content-Length']
        fsize = fpath.stat().st_size
        if int(length) <= fsize:
            print("File " + fname + " exists: Skipping.\n")
            return 1

    chunk_size = 1024 * 512  # 512 KB
    dl = session.get(url, stream=True, allow_redirects=True)
    with open(fname, "wb") as fout:
        with tqdm(unit="B", unit_scale=True, unit_divisor=1024, miniters=1,
                  desc=fname, total=int(dl.headers.get('content-length'))
                  ) as pbar:
            for chunk in dl.iter_content(chunk_size=chunk_size):
                pbar.update(fout.write(chunk))


def process_shoot(url):
    soup = get_html(url)
    files = get_download_urls(soup)
    metadata = get_metadata(soup)

    fname = re.sub('[^a-zA-Z0-9 \n\.]', '', metadata['channel']) + \
            "__" + re.sub('[^a-zA-Z0-9 \n\.]', '', metadata['title']) + \
            "___" + args.quality
    fname = fname.lower().replace(" ", "_")
    
    write_metadata_nfo(metadata, fname + ".nfo")
    download_file(files['poster'], fname + ".jpg")
    download_file(files[args.quality], fname + ".mp4")


def process_gallery(url):
    soup = get_html(url)
    base_url = url.split("page/")[0].split("?")[0]
    search_filter = url.split("?")[-1]
    shoot_urls = get_shoot_url_list(soup)
    if args.recursive is True:
        total_pages = int(soup.find("div", "pagination").find_all("a")[-1].text)
        if "/page/" in url:
            next_page = int(url.split("/")[6]) + 1
        else:
            next_page = 2
        while next_page <= total_pages:
            next_url = base_url + "page/" + str(next_page) + "/?" + search_filter
            shoot_urls += get_shoot_url_list(get_html(next_url))
            next_page += 1
    for shoot in shoot_urls:
        process_shoot(shoot)


def main():
    if args.quality is None:
        quality = "1080"
    else:
        quality = args.quality

    # Grab shoot url from commandline arguments
    url = args.url
    process_gallery(url)


if __name__ == '__main__':
    main()
